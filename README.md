# MidiLOF


## Description
MidiLOF is an “app” for the Novation Launchpad Pro that provides eight MIDI CC LFOs. CC goes for Continuous Controller and LFO is a low frequency oscillator that produces changing values over time.

![alt text](img/MidiLOF-img1.png "MidiLOF App")

The Main page of the MidiLFO App

The main features of MidiLOF are:
- Eight individually configurable MIDI CC LFOs
- Vertical fader bars on the 8x8 pad matrix graphically represent the LFO values
- LFOs can be configured by
    - Waveform (sin, saw, triangle, rectangle, S&H, S&H triangle), frequency,
    - CC destination address,
    - Lower/upper bound of CC values, step width
    - MIDI channel/port
    - Beside others.
- Each configuration parameter can be changed while running.
- Global and LFO support individual starting, pausing, continuing and stopping.
- In global pause mode, stepping backward and forward in time is supported, to find good CC constellations.
- Four presets can be stored on the Launchpad Pro.
- Working preset can be saved/recalled using SysEx commands.
- MIDI Thru to enable Note On/Off forwarding, especially from DIN in to DIN out.
- All control and parameter changes are made from the Launchpad Pro.
- Direct manipulation of LFO frequency, lower/upper bound on the main page.

New in Version 1.10
- MIDI real-time Message support: Clock, Start/Stop.
- BPM query and change.
- Bug fixes.

New in Version 1.11
- Improvements in accuracy.
- Bug fixes.


## Videos
A YouTube channel is available: [https://www.youtube.com/@MidiLOF](https://www.youtube.com/@MidiLOF).

## Version
This is version V110. The Zip file name is MidiLOF-V110.zip

MD5 (MidiLOF-V111.zip) = 979e4dfe24780b6d43d18c97d92b1a78

## Historical Versions
Historical Versions are available under the tags section.

## Download
The only thing needed is the MidiLOF_Vxxx.zip file. It contains a directory with two files. 

LPP-MidiLOF-Vxxx
- LPP-MidiLOF-Vxxx.syx (the SysEx file of the MidiLOF “App”)
- Manual-MidiLOF-Vxxx.pdf (the manual)

Downloading can be down by selecting the MidiLOF_Vxxx.zip file and press the theDownload link. 

## Installation
Installation is explained in the user manual.

## Usage
A user manual is provided to explain the MidiLOF app.

## Support
At the moment no direct support is available. Issues can be raised using the GitLab issue tracker. The Facebook group: "MidiLOF Community" can also be used.

## Roadmap
Ready for release.

## Contributing
This is a closed Source project. But I am open for ideas.

## Authors and acknowledgment
MidiLFO is developed using the Open source firmware provided by Novation. See (https://github.com/dvhdr/launchpad-pro)

## License
See the License file.

## Source Code
The Source Code is not available.

## Project status
Productive

